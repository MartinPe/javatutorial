package Collections5_Sets;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Main {

    public static void main(String[] args){

        // simple hashset does not retain orde

        // Set<String> set1 = new HashSet<String>();

        // linkedhashset
        //Set<String> set1 = new LinkedHashSet<>();

        Set<String> set1 = new TreeSet<>();

        if(set1.isEmpty()) {
            System.out.println("Lista jest pusta");
        }
        set1.add("mouse");
        set1.add("cat");
        set1.add("elefant");
        set1.add("dog");
        set1.add("horse");
        set1.add("camel");

        if(set1.isEmpty()) {
            System.out.println("Lista jest pusta");
        }

        // adding duplicate does nothing

        set1.add("camel");

        // System.out.println(set1);

        //////////////for////////////////

//        for(String e: set1){
//            System.out.println(e);
//        }

        // Does set contains a given item?///

        if(set1.contains("mouse")){
            System.out.println("contains mouse");

            Set<String> set2 = new TreeSet<>();

            set2.add("duck");
            set2.add("cock");
            set2.add("girafe");
            set2.add("rabbit");
            set2.add("pelikan");
            set2.add("dog");

            Set<String> intersection = new HashSet<>(set1);

            System.out.println(intersection);

            intersection.retainAll(set2);

            System.out.println(intersection);

            Set<String> difference = new HashSet<>(set2);

            difference.removeAll(set1);

            System.out.println(difference);







        }
    }
}
