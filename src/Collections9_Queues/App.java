package Collections9_Queues;

import java.util.NoSuchElementException;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

public class App {

    public static void main(String[] args) {

        // head<Integer> <---- oooooooooo < - tail FIFO(first in, first out)

        Queue<Integer> q1 = new ArrayBlockingQueue<>(3);

        // Throws NoSuchElement exception or no items in queue yet
        // System.out.println("Head of queue is; " + q1.element());

        q1.add(1);
        q1.add(2);
        q1.add(10);
        try {
            q1.add(3);
        } catch (IllegalStateException e){
            System.out.println("Too many items");
        }


        for( Integer val: q1){
            System.out.println(val);
        }


        System.out.println("Removed from queue:"+ q1.remove());
        System.out.println("Removed from queue:"+ q1.remove());
        System.out.println("Removed from queue:"+ q1.remove());
        try {
            System.out.println("Removed from queue:"+ q1.remove());
        }catch (NoSuchElementException e){
            System.out.println("Try remove too many items from queue");
        }


        Queue<Integer> q2 = new ArrayBlockingQueue<>(3);


        q2.offer(10);
        q2.offer(20);

        if(q2.offer(30)== false){
            System.out.println("Offer failed to add tird item.");
        }

        for(Integer value: q2){
            System.out.println(value);
        }

        // poll - removes items

        System.out.println(q2.poll());
        System.out.println(q2.poll());
        System.out.println(q2.poll());

        //peek
        System.out.println(q2.peek());
    }
}
