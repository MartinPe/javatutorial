package Collections8_Natural_Ordering;


import java.util.*;

class Person implements Comparable<Person>{

    private String name;

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                '}';
    }


    public Person(String name){
        this.name = name;
    }


    @Override
    public int compareTo(Person person) {
        return name.compareTo(person.name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}

public class App {

    public static void main(String[] args) {

        List<Person> list = new ArrayList<>();

        SortedSet<Person> set = new TreeSet<>();

        addElement(list);
        addElement(set);

      Collections.sort(list);

        showElements(list);
        System.out.println("");

        showElements(set);
    }

    private static void addElement(Collection <Person> col){
        col.add(new Person("Joe"));
        col.add(new Person("Sue"));
        col.add(new Person("Build"));
        col.add(new Person("Sthephan"));
        col.add(new Person("Romea"));
        col.add(new Person("Last"));
    }

    private static void showElements(Collection<Person> col){
        for(Person element: col){
            System.out.println(element);
        }
    }
}
