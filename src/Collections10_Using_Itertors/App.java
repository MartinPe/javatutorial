package Collections10_Using_Itertors;

import java.util.Iterator;
import java.util.LinkedList;

public class App {

    public static void main(String[] args) {
        LinkedList<String> animals = new LinkedList<>();

        animals.add("fox");
        animals.add("dog");
        animals.add("cat");

        // Iterators that allows you to make some filters in list np. remove all cat

        Iterator<String> it= animals.iterator();

        while (it.hasNext()) {
            String anima = it.next();

            System.out.println(anima);

            if (anima.equals("cat")){
                it.remove();
            }
        }
        System.out.println("");


        //Modern iterator, Java 5 and later

        for(String animal: animals){
            System.out.println(animal);
        }
    }

}
