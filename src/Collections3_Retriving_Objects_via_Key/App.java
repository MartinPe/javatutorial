package Collections3_Retriving_Objects_via_Key;

import java.util.HashMap;
import java.util.Map;

public class App {
    public static void main(String[] args) {

        HashMap< Integer, String> map = new HashMap<>();

        map.put(5, "five");
        map.put(7, "seven");
        map.put(6, "six");
        map.put(4, "four");

        map.put(6,"hello");


        String text = map.get(6);
        System.out.println(text);

        for(Map.Entry<Integer, String> entry: map.entrySet()){
            int key = entry.getKey();
            String value = entry.getValue();

            System.out.println(key + ":" + value);
        }

    }
}
