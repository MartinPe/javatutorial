package Collections7_Sorting;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class App {
    public static void main(String[] args) {

        List<String> animals = new ArrayList<String>();

        animals.add("cat");
        animals.add("dog");
        animals.add("lion");
        animals.add("mongose");
        animals.add("snake");

        Collections.sort(animals);

        for (String animal: animals){
            System.out.println(animal);
        }

        List<Integer> numbers =  new ArrayList<Integer>();

        numbers.add(14);
        numbers.add(43);
        numbers.add(53);
        numbers.add(34);

     }
}
