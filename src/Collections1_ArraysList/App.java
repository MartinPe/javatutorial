package Collections1_ArraysList;

import java.security.AlgorithmConstraints;
import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main(String[] args) {

        ArrayList<Integer> numbers = new ArrayList<Integer>();

        // Adding
        // you can put only non-primitive type
        numbers.add(10);
        numbers.add(100);
        numbers.add(40);
        numbers.add(70);

        // Retrieving
        System.out.println(numbers.get(1));

        // Indexed for loop iteration(Iteration nr 1)

        for (int i = 0; i < numbers.size(); i++) {
            System.out.println(numbers.get(i));
        }

        for (Integer value : numbers) {
            System.out.println(value);
        }

        // Removing items (careful!)
        numbers.remove(numbers.size() - 1);

        //This is very slow(0), because he has to copy all the list to remove first item
        numbers.remove(0);

        System.out.println("nIteration #2");

        for (Integer value : numbers) {
            System.out.println(value);
        }
        // List interface...

        List<String> values = new ArrayList<String>();

    }
}
