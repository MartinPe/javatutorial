package Collections6_Adding_Objects_to_Maps;

import java.util.*;

public class Exce{

    public static void main(String[] args) {


        Person p1 = new Person(0,"Bob");
        Person p2 = new Person(1,"Andrew");
        Person p3 = new Person(0,"Bob");
        Person p4 = new Person(3,"Roy");

        Map<Person, Integer> map = new LinkedHashMap<Person, Integer>();

        map.put(p1, 1);
        map.put(p2, 2);
        map.put(p3, 3);
        map.put(p4, 4);

        for(Person key: map.keySet()){
            System.out.println(key + ":" + map.get(key));
        }

        Set<Person> set1 = new LinkedHashSet<Person>();

        set1.add(p1);
        set1.add(p2);
        set1.add(p3);
        set1.add(p4);

        System.out.println(set1) ;
    }
}
