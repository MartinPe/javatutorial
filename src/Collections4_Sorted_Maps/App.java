package Collections4_Sorted_Maps;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;


public class App {


    public static void main(String[] args) {

        Map<Integer, String> hashMap = new HashMap<>();
        Map<Integer, String> linkedHashMap = new LinkedHashMap<>();
        Map<Integer, String> treeMap = new TreeMap<>();

        testMap(linkedHashMap);

    }

    public static void testMap(Map<Integer, String> map) {
        map.put(9, "fox");
        map.put(4, "cat");
        map.put(8, "dog");
        map.put(5, "horse");
        map.put(2, "mouse");
        map.put(3, "daniel");
        map.put(1, "cock");

        for(Integer key: map.keySet()){
            String value = map.get(key);

            System.out.println(key +": "+ value);
        }
    }
}
